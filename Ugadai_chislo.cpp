#include <stdio.h>
#include <time.h>
#include <stdlib.h>


int main() {
	srand(time(NULL));
	int i=0, count=0;
	int num = rand()%100;
	puts("Try to guess the number:\n");
	scanf("%d", &i);
	while (i!=num)
	{
		if (i>num) puts("The number is <");
		else puts ("The number is >");
		puts("Try to guess the number:\n");
		scanf("%d", &i);
		count++;		
	}
	printf("You guess %d\n", i);
	printf("You guess in %d -th time", count);
	return 0;
}